package agency;

/**
 * Created by Tadeas Kucera (423907) on 10.3.2016.
 */
public class Hero {
    private Long id;
    private String name;
    private String nickName;
    private Ability ability;

    public Hero(String name, String nickName, Ability ability) {
        this.name = name;
        this.nickName = nickName;
        this.ability = ability;
    }

    public Hero() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Ability getAbility() {
        return ability;
    }

    public void setAbility(Ability ability) {
        this.ability = ability;
    }



    @Override
    public String toString() {
        return "Hero{" +
                "id=" + id +
                ", name=" + name +
                ", nick name=" + nickName +
                ", ability=" + ability +
                '}';
        //to string
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hero hero = (Hero) o;

        return id != null ? id.equals(hero.id) : hero.id == null;
        //pokud maji oba null id, vrati to true

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

}
