package agency;

import java.sql.Time;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Created by linda on 10.03.2016.
 */
public class Mission {

    Long id;
    String description;
    Ability abilityRequired;
    LocalDate endTime;
    boolean successful = false;
    Long doneByHeroID = -1l;

    public Mission(String description, Ability abilityRequired, LocalDate endTime) {
        this.description = description;
        this.abilityRequired = abilityRequired;
        this.endTime = endTime;
    }

    public Mission() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Ability getAbilityRequired() {
        return abilityRequired;
    }

    public void setAbilityRequired(Ability abilityRequired) {
        this.abilityRequired = abilityRequired;
    }

    public LocalDate getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDate endTime) {
        this.endTime = endTime;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public Long getDoneByHeroID() {
        return doneByHeroID;
    }

    public void setDoneByHeroID(Long doneByHeroID) {
        this.doneByHeroID = doneByHeroID;
    }

    public void setEndTime(Time endTime) {
        this.endTime = LocalDate.from(endTime.toLocalTime());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (this.id == null && this != obj) {
            return false;
        }
        final Mission other = (Mission) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

}