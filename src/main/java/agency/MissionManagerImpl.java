package agency;

import exceptions.EntityNotFoundException;
import exceptions.ServiceFailureException;
import utils.DBManager;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Linda Langerova on 16.03.2016.
 * @version 0.2
 */
public class MissionManagerImpl implements MissionManager {

    private DataSource dataSource;

    public MissionManagerImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    public MissionManagerImpl(){dataSource = null;}

    @Override
    public void createMission(Mission mission) throws ServiceFailureException {

        if(mission==null) throw new IllegalArgumentException("mission should not be null");
        if(mission.getId()!=null) throw new IllegalArgumentException("mission id should not be assigned prior saving");
        validate(mission);

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "INSERT INTO MISSION (description,abilityRequired,endTime,successful,doneByHero) VALUES (?,?,?,?,?)",
                        Statement.RETURN_GENERATED_KEYS)) {

            st.setString(1, mission.getDescription());
            st.setString(2, mission.getAbilityRequired().toString());
            st.setDate(3, toSqlDate(mission.getEndTime()));
            st.setBoolean(4,false);
            st.setLong(5,-1l);

            int addedRows = st.executeUpdate();
            if (addedRows != 1) {
                throw new ServiceFailureException("Internal Error: More rows ("
                        + addedRows + ") inserted when trying to insert mission " + mission);
            }

            ResultSet keyRS = st.getGeneratedKeys();
            mission.setId(getKey(keyRS, mission));

        } catch (SQLException ex) {
            throw new ServiceFailureException("Error when inserting mission " + mission.toString(), ex);
        }

    }

    private void checkDataSource() {
        if (dataSource == null) {
            throw new IllegalStateException("DataSource is not set");
        }
    }

    @Override
    public void updateMission(Mission mission) throws ServiceFailureException {

        validate(mission);
        if (mission.getId() == null) {
            throw new IllegalArgumentException("mission id is null");
        }
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "UPDATE mission SET description = ?, abilityRequired = ?, endTime = ?, " +
                                "successful = ?, doneByHero = ? WHERE id = ?")) {

            st.setString(1, mission.getDescription());
            st.setString(2, mission.getAbilityRequired().toString());
            st.setDate(3, Date.valueOf(String.valueOf(mission.getEndTime())));
            st.setBoolean(4, mission.isSuccessful());
            st.setLong(5, mission.getDoneByHeroID());
            st.setLong(6, mission.getId());

            int count = st.executeUpdate();
            if (count == 0) {
                throw new EntityNotFoundException("Mission " + mission + " was not found in database!");
            } else if (count != 1) {
                throw new ServiceFailureException("Invalid updated rows count detected (one row should be updated): " + count);
            }
        } catch (SQLException ex) {
            throw new ServiceFailureException(
                    "Error when updating mission " + mission, ex);
        }
    }

    @Override
    public void deleteMission(Mission mission) throws ServiceFailureException {

        if(mission == null) {
            throw new IllegalArgumentException("Cannot delete mission that is null.");
        }
        if(mission.getId() == null) {
            throw new IllegalArgumentException("Cannot delete mission with null id.");
        }
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "DELETE FROM mission WHERE id = ?")
        ) {
            st.setLong(1, mission.getId());

            int count = st.executeUpdate();
            switch(count) {
                case 1: break;
                case 0: throw new EntityNotFoundException("Mission " + mission + " was not found in database!");
                default: throw new ServiceFailureException("Invalid rows to be deleted count detected: " + count);
            }

        } catch (SQLException e) {
            throw new ServiceFailureException("Error when deleting mission " + mission + e);
        }
    }

    @Override
    public Mission findMissionByID(Long id) throws ServiceFailureException {

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "SELECT id,description,abilityRequired,endTime,doneByHero,successful FROM mission WHERE id = ?")) {

            st.setLong(1, id);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                Mission mission = resultSetToMission(rs);

                if (rs.next()) {
                    throw new ServiceFailureException(
                            "Internal error: More entities with the same id found "
                                    + "(source id: " + id + ", found " + mission + " and " + resultSetToMission(rs));
                }
                return mission;
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new ServiceFailureException(
                    "Error when retrieving mission with id " + id, e);
        }
    }

    @Override
    public List<Mission> listAllMissions() throws ServiceFailureException {


        Connection connection = null;
        PreparedStatement st = null;
        try {
            connection = dataSource.getConnection();
            st = connection.prepareStatement(
                    "SELECT id, description, abilityRequired, endTime, successful, doneByHero FROM mission");
            return executeQueryForMultipleMissions(st);
        } catch (SQLException ex) {
            String msg = "Error when getting all missions from DB";
            throw new ServiceFailureException(msg, ex);
        } finally {
            DBManager.closeQuietly(connection, st);
        }
    }

    @Override
    public void markAsSuccessful(Mission mission) throws ServiceFailureException {

        mission.setSuccessful(true);
        updateMission(mission);
    }

    @Override
    public List<Mission> listMissionsThatHaveToBeDone() throws ServiceFailureException {

        List<Mission> missions = new ArrayList<>();

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "SELECT * FROM mission WHERE successful = false")) {

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Mission mission = resultSetToMission(rs);
                missions.add(mission);
            }
        } catch (SQLException e) {
            throw new ServiceFailureException(
                    "Error when retrieving missions from database " + e);
        }
        return missions;
    }

    private void validate(Mission mission) throws IllegalArgumentException {
        if (mission == null) {
            throw new IllegalArgumentException("Mission is null");
        }
        if (mission.getDescription() == null) {
            throw new IllegalArgumentException("Mission must have description");
        }
        if (mission.getAbilityRequired() == null) {
            throw new IllegalArgumentException("Mission must be finishable by a special ability");
        }
        if (mission.getEndTime() == null) {
            throw new IllegalArgumentException("Mission EndTime can't be never");
        }
        if (mission.getEndTime().isBefore(ChronoLocalDate.from(LocalDate.now()))) {
            throw new IllegalArgumentException("Mission EndTime can't expire before adding into database");
        }
        if (mission.getDoneByHeroID() != -1l) {
            throw new IllegalArgumentException("Mission can't be finished before adding into database");
        }
    }

    private Long getKey(ResultSet keyRS, Mission mission) throws ServiceFailureException, SQLException {
        if (keyRS.next()) {
            if (keyRS.getMetaData().getColumnCount() != 1) {
                throw new ServiceFailureException("Internal Error: Generated key"
                        + "retriving failed when trying to insert mission " + mission
                        + " - wrong key fields count: " + keyRS.getMetaData().getColumnCount());
            }
            Long result = keyRS.getLong(1);
            if (keyRS.next()) {
                throw new ServiceFailureException("Internal Error: Generated key"
                        + "retriving failed when trying to insert mission " + mission
                        + " - more keys found");
            }
            return result;
        } else {
            throw new ServiceFailureException("Internal Error: Generated key"
                    + "retriving failed when trying to insert mission " + mission
                    + " - no key found");
        }
    }

    static Mission executeQueryForSingleMission(PreparedStatement st) throws SQLException, ServiceFailureException {
        ResultSet rs = st.executeQuery();
        if (rs.next()) {
            Mission result = resultSetToMission(rs);
            if (rs.next()) {
                throw new ServiceFailureException(
                        "Internal integrity error: more missions with the same id found!");
            }
            return result;
        } else {
            return null;
        }
    }

    static List<Mission> executeQueryForMultipleMissions(PreparedStatement st) throws SQLException {
        ResultSet rs = st.executeQuery();
        List<Mission> result = new ArrayList<Mission>();
        while (rs.next()) {
            result.add(resultSetToMission(rs));
        }
        return result;
    }

    private static Mission resultSetToMission(ResultSet rs) throws SQLException {
        Mission mission = new Mission();
        mission.setId(rs.getLong("id"));
        mission.setAbilityRequired(Ability.valueOf(rs.getString("abilityRequired")));
        mission.setDescription(rs.getString("description"));

        mission.setEndTime(rs.getDate("endTime").toLocalDate());
        mission.setDoneByHeroID(rs.getLong("doneByHero"));

        mission.setSuccessful(rs.getBoolean("successful"));
        return mission;
    }

    private static Date toSqlDate(LocalDate localDate) {
        return localDate == null ? null : Date.valueOf(localDate);
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}