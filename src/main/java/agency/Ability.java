package agency;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Tadeas Kucera (423907) on 10.3.2016.
 */
public enum Ability {
    FIRE(1), WATER(2), BAT(3), BOOK(4), MATH(5);

    private int abilityIndex;

    private static Map<Integer, Ability> map = new HashMap<>();

    static {
        for (Ability abil : Ability.values()) {
            map.put(abil.abilityIndex, abil);
        }
    }

    private Ability(final int index) {
        abilityIndex = index;
    }

    public static Ability valueOf(int index) {
        return map.get(index);
    }

}