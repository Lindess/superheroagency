package agency;

import exceptions.IllegalEntityException;
import exceptions.ServiceFailureException;
import utils.DBManager;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Tadeas Kucera (423907) on 24.3.2016.
 */

/*
resultSetToHero is private thus cannot be used outside the HMImpl class(same for resultSetToMission). Options:
1) set it public
2) let SLMImpl to extend from HMImpl and MMImpl both
3) copy code resultSetToHero inside SLMImpl (current version)
 */
public class StanLeeManagerImpl implements StanLeeManager {

    private static final Logger logger = Logger.getLogger(
            MissionManagerImpl.class.getName());

    private DataSource dataSource;

    public StanLeeManagerImpl(DataSource dataSource) { this.dataSource = dataSource; }
    public StanLeeManagerImpl() {}

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private void checkDataSource() {
        if (dataSource == null) {
            throw new IllegalStateException("DataSource is not set");
        }
    }

    @Override//podle Adamka
    public void sendHeroOnMission(Hero hero, Mission mission) throws ServiceFailureException {
        checkDataSource();
        if (mission == null) {
            throw new IllegalArgumentException("mission is null");
        }
        if (mission.getId() == null) {
            throw new IllegalEntityException("mission id is null");
        }
        if (hero == null) {
            throw new IllegalArgumentException("hero is null");
        }
        if (hero.getId() == null) {
            throw new IllegalEntityException("hero id is null");
        }
        Connection conn = null;
        PreparedStatement updateSt = null;
        try {
            conn = dataSource.getConnection();
            // Temporary turn autocommit mode off. It is turned back on in
            // method DBUtils.closeQuietly(...)
            conn.setAutoCommit(false);

            if(hero.getAbility().equals(mission.getAbilityRequired())) {

                checkIfMissionIsActive(conn, mission);
                updateSt = conn.prepareStatement("UPDATE Mission SET doneByHero = ?, successful = TRUE WHERE id = ?");
                updateSt.setLong(1, hero.getId());
                updateSt.setLong(2, mission.getId());
                int count = updateSt.executeUpdate();
                if (count == 0) {
                    throw new IllegalEntityException("Mission " + mission + " not found or it is already done or expired");
                }
                DBManager.checkUpdatesCount(count, hero, false);

                conn.commit();

            }
        } catch (SQLException ex) {
            String msg = "Error when sending hero on mission";
            logger.log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        } finally {
            DBManager.doRollbackQuietly(conn);
            DBManager.closeQuietly(conn, updateSt);
        }
    }

    @Override
    public void sendHeroOnMission(Long heroID, Long missionID) throws ServiceFailureException {
        MissionManager mm = new MissionManagerImpl(dataSource);
        HeroManager hm = new HeroManagerImpl(dataSource);
        Mission mission = mm.findMissionByID(missionID);
        Hero hero = hm.findHeroByID(heroID);
        sendHeroOnMission(hero, mission);
    }

    //podobne jako Adamek
    private void checkIfMissionIsActive(Connection conn, Mission mission) throws IllegalEntityException, SQLException {
        PreparedStatement checkSt = null;
        try {
            checkSt = conn.prepareStatement(
                    "SELECT successful, endTime " +
                            "FROM mission " +
                            "WHERE mission.id = ? ");
            checkSt.setLong(1, mission.getId());
            ResultSet rs = checkSt.executeQuery();
            if (rs.next()) {
                if (rs.getBoolean("successful")) {
                    throw new IllegalEntityException("Mission " + mission + " is already successful");
               // } else if (LocalDate.toLocalDate(rs.getDate("endTime")).isNow ) {
                 //       throw new IllegalEntityException("Mission " + mission + " has expired");
                    }
                } else {
                throw new IllegalEntityException("Mission " + mission + " does not exist in the database");
            }
        } finally {
            DBManager.closeQuietly(null, checkSt);
        }
    }


    //podobne jako Adamek
    @Override
    public List<Mission> findMissionsDoneByHero(Hero hero) throws ServiceFailureException, IllegalEntityException {
        checkDataSource();
        if (hero == null) {
            throw new IllegalArgumentException("hero is null");
        }
        if (hero.getId() == null) {
            throw new IllegalEntityException("hero id is null");
        }
        PreparedStatement st = null;
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement(
                    "SELECT id, abilityRequired, description, endTime, doneByHero, successful " +
                            "FROM mission WHERE donebyhero = ?");
            st.setLong(1, hero.getId());
            return MissionManagerImpl.executeQueryForMultipleMissions(st);
        } catch (SQLException ex) {
            String msg = "Error when trying to find missions done by hero " + hero;
            logger.log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        } finally {
            DBManager.closeQuietly(conn, st);
        }
    }


    //podobne jako Adamek
    @Override
    public List<Mission> findMissionsForSpecifiedHero(Hero hero) throws ServiceFailureException {
        checkDataSource();
        PreparedStatement st = null;
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement(
                    "SELECT id, abilityRequired, description, endTime, doneByHero, successful " +
                            " FROM mission WHERE abilityRequired = ? AND successful = false");

            st.setString(1, hero.getAbility().toString());
            return MissionManagerImpl.executeQueryForMultipleMissions(st);
        } catch (SQLException ex) {
            String msg = "Error when trying to find missions for specified hero " + hero;
            logger.log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        } finally {
            DBManager.closeQuietly(conn, st);
        }
    }

    @Override
    public List<Hero> findHeroSuitableForMission(Mission mission) throws ServiceFailureException {
        checkDataSource();
        PreparedStatement st = null;
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement(
                    "SELECT id,name,nickname,ability FROM hero WHERE ability = ?");
            st.setString(1, mission.getAbilityRequired().toString());
            return HeroManagerImpl.executeQueryForMultipleHeroes(st);
        } catch (SQLException ex) {
            String msg = "Error when trying to find heroes that are capable of completing mission " + mission;
            logger.log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        } finally {
            DBManager.closeQuietly(conn, st);
        }
    }

    @Override //podle Adamka
    public List<Mission> listMissionsThatHaveToBeDone() throws ServiceFailureException {
        checkDataSource();

        PreparedStatement st = null;
        Connection connection = null;
        try {
                connection = dataSource.getConnection();
                st = connection.prepareStatement(
                        "SELECT id, abilityRequired, description, endTime, doneByHero, successful " +
                                "FROM mission WHERE successful = false AND doneByHero = -1");
                //doneByHero isNotNULL in SQL?? -> doneByHero is -1 when it's not done
                return MissionManagerImpl.executeQueryForMultipleMissions(st);
        } catch (SQLException ex) {
            String msg = "Error when trying to find missions that have to be done.";
            logger.log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        } finally {
            DBManager.closeQuietly(connection, st);
        }
    }
}