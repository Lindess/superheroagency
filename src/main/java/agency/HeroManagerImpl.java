package agency;

import exceptions.EntityNotFoundException;
import exceptions.ServiceFailureException;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Tadeas Kucera (423907) on 17.3.2016.
 * Updated by Linda Langerova (445710) on 22.3.2016
 *
 */
public class HeroManagerImpl implements HeroManager {

    private DataSource dataSource;

    public HeroManagerImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    public HeroManagerImpl() {dataSource = null;}


    @Override
    public void createHero(Hero hero) {
        if(hero==null) throw new IllegalArgumentException("hero should not be null");
        if(hero.getId()!=null) throw new IllegalArgumentException("hero id should not be assigned prior saving");

        validate(hero);
        if (hero.getId() != null) {
            throw new IllegalArgumentException("hero id is already set");
        }
        checkDataSource();
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "INSERT INTO HERO (name,nickname,ability) VALUES (?,?,?)",
                        Statement.RETURN_GENERATED_KEYS)) {

            st.setString(1, hero.getName());
            st.setString(2, hero.getNickName());
            st.setString(3, hero.getAbility().toString());
            int addedRows = st.executeUpdate();
            if (addedRows != 1) {
                throw new ServiceFailureException("Internal Error: More rows ("
                        + addedRows + ") inserted when trying to insert hero " + hero);
            }

            ResultSet keyRS = st.getGeneratedKeys();
            hero.setId(getKey(keyRS, hero));

        } catch (SQLException ex) {
            throw new ServiceFailureException("Error when inserting hero " + hero, ex);
        }
    }

    private void checkDataSource() {
        if (dataSource == null) {
            throw new IllegalStateException("DataSource is not set");
        }
    }

    @Override
    public void updateHero(Hero hero) throws ServiceFailureException {
        validate(hero);
        if (hero.getId() == null) {
            throw new IllegalArgumentException("hero id is null");
        }
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "UPDATE hero SET nickname = ?, name = ?, ability = ?, WHERE id = ?")) {

            st.setString(1, hero.getNickName());
            st.setString(2, hero.getName());
            st.setString(3, hero.getAbility().toString());
            st.setLong(4, hero.getId());

            int count = st.executeUpdate();
            if (count == 0) {
                throw new EntityNotFoundException("Hero " + hero + " was not found in database!");
            } else if (count != 1) {
                throw new ServiceFailureException("Invalid updated rows count detected (one row should be updated): " + count);
            }
        } catch (SQLException ex) {
            throw new ServiceFailureException(
                    "Error when updating hero " + hero, ex);
        }
    }

    @Override
    public void deleteHero(Hero hero) throws ServiceFailureException {
        if(hero == null) {
            throw new IllegalArgumentException("Cannot delete hero that is null.");
        }
        if(hero.getId() == null) {
            throw new IllegalArgumentException("Cannot delete hero with null id.");
        }
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement(
                "DELETE FROM hero WHERE id = ?")
                ) {
            st.setLong(1, hero.getId());

            int count = st.executeUpdate();
            switch(count) {
                case 1: break;
                case 0: throw new EntityNotFoundException("Hero " + hero + " was not found in database!");
                default: throw new ServiceFailureException("Invalid rows to be deleted count detected: " + count);
            }

        } catch (SQLException e) {
            throw new ServiceFailureException("Error when deleting hero " + hero +e);
        }
    }



    @Override
    public List<Hero> findAllHeroes() throws ServiceFailureException {

        List<Hero> heroes = new ArrayList<>();

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "SELECT * FROM hero")) {

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Hero hero = resultSetToHero(rs);
                heroes.add(hero);
            }
        } catch (SQLException e) {
            throw new ServiceFailureException(
                    "Error when retrieving heroes from database " + e);
        }
        return heroes;
    }

    @Override
    public Hero findHeroByID (Long id) throws ServiceFailureException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "SELECT id,nickname,name,ability FROM hero WHERE id = ?")) {

            st.setLong(1, id);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                Hero hero = resultSetToHero(rs);

                if (rs.next()) {
                    throw new ServiceFailureException(
                            "Internal error: More entities with the same id found "
                                    + "(source id: " + id + ", found " + hero + " and " + resultSetToHero(rs));
                }
                
                return hero;
            } else {
                return null;
            }

        } catch (SQLException e) {
            throw new ServiceFailureException(
                    "Error when retrieving hero with id " + id, e);
        }
    }


    @Override
    public Hero findHeroByNick(String nick) throws ServiceFailureException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "SELECT id, nickname, name, ability FROM hero WHERE nickname = ?")
                ) {
            st.setString(1, nick); //naplnim prvni otaznicek obsahem promenne nick
            ResultSet rs = st.executeQuery(); //spustim SQL prikaz st v databazi a vyslednou tabulku dam do rs

            if(rs.next()) {
                return resultSetToHero(rs);
            } else {
                return null;
            }

        } catch (SQLException e) {
            throw new ServiceFailureException(
                    "Error when retrieving hero with nick " + nick, e);
        }
    }


    private void validate(Hero hero) throws IllegalArgumentException {
        if (hero == null) {
            throw new IllegalArgumentException("Hero is null");
        }
        if (hero.getAbility() == null) {
            throw new IllegalArgumentException("Hero has no special ability. What a hero!");
        }
        if (hero.getNickName() == null) {
            throw new IllegalArgumentException("Hero has no nickname");
        }
    }

    private Long getKey(ResultSet keyRS, Hero hero) throws ServiceFailureException, SQLException {
        if (keyRS.next()) {
            if (keyRS.getMetaData().getColumnCount() != 1) {
                throw new ServiceFailureException("Internal Error: Generated key"
                        + "retrieving failed when trying to insert hero " + hero
                        + " - wrong key fields count: " + keyRS.getMetaData().getColumnCount());
            }
            Long result = keyRS.getLong(1);
            if (keyRS.next()) {
                throw new ServiceFailureException("Internal Error: Generated key"
                        + "retrieving failed when trying to insert hero " + hero
                        + " - more keys found");
            }
            return result;
        } else {
            throw new ServiceFailureException("Internal Error: Generated key"
                    + "retrieving failed when trying to insert hero " + hero
                    + " - no key found");
        }
    }

    static List<Hero> executeQueryForMultipleHeroes(PreparedStatement st) throws SQLException {
        ResultSet rs = st.executeQuery();
        List<Hero> result = new ArrayList<Hero>();
        while (rs.next()) {
            result.add(resultSetToHero(rs));
        }
        return result;
}

    static Hero executeQueryForSingleHero(PreparedStatement st) throws SQLException {
        ResultSet rs = st.executeQuery();
        if (rs.next()) {
            Hero result = resultSetToHero(rs);
            if (rs.next()) {
                throw new ServiceFailureException(
                        "Internal integrity error: more heroes with the same id found!");
            }
            return result;
        } else {
            return null;
        }
    }

    private static Hero resultSetToHero(ResultSet rs) throws SQLException {
        Hero hero = new Hero();
        hero.setId(rs.getLong("id"));
        hero.setName(rs.getString("name"));
        hero.setNickName(rs.getString("nickname"));
        hero.setAbility(Ability.valueOf(rs.getString("ability")));
        return hero;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

}
