package agency;

import exceptions.ServiceFailureException;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created by linda on 16.03.2016.
 */
public interface MissionManager {

    /**
     * Stores new mission into database. Id for the new hero is automatically
     * generated and stored into id attribute.
     *
     * @param mission mission which we are creating.
     * @throws IllegalArgumentException when mission is null, or mission has already assigned id.
     * @throws ServiceFailureException when database operation fails.
     */
    void createMission(Mission mission) throws ServiceFailureException;

    /**
     * Updates mission.
     *
     * @param mission mission that will be updated.
     * @throws IllegalArgumentException when mission is null, or it has null id.
     * @throws ServiceFailureException when database operation fails.
     */
    void updateMission(Mission mission) throws ServiceFailureException;

    /**
     * Deletes mission from database.
     *
     * @param mission mission which we are deleting.
     * @throws IllegalArgumentException when mission is null, or it has null id.
     * @throws ServiceFailureException when database operation fails.
     */
    void deleteMission(Mission mission) throws ServiceFailureException;

    /**
     * Returns mission with specified id from database.
     *
     * @param id id of mission that we are looking for.
     * @return Mission with specified id, or null when there is no mission with that id.
     * @throws ServiceFailureException when database operation fails.
     */
    Mission findMissionByID(Long id) throws ServiceFailureException;

    /**
     * Lists all missions in database.
     *
     * @return List of all missions.
     * @throws ServiceFailureException when database operation fails.
     */
    List<Mission> listAllMissions() throws ServiceFailureException;

    /**
     * Marks mission as successfully done.
     *
     * @param mission mission which should be marked.
     * @throws IllegalArgumentException when mission is null, it has null id, or the mission is already done.
     * @throws ServiceFailureException when database operation fails.
     */
    void markAsSuccessful(Mission mission) throws ServiceFailureException;

    /**
     * Lists all missions from database these weren't done and theirs time haven't elapsed yet.
     *
     * @return All missions that have to be done.
     * @throws ServiceFailureException when database operation fails.
     */
    List<Mission> listMissionsThatHaveToBeDone() throws ServiceFailureException;

    public void setDataSource(DataSource dataSource);

}
