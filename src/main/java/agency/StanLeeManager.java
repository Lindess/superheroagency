package agency;

import exceptions.ServiceFailureException;

import java.util.List;

/**
 * Created by linda on 16.03.2016.
 */
public interface StanLeeManager {
    /**
     * Sends hero on specified mission, but only if the mission is not succesful yet and if the ability of the hero
     * match the requirement.
     *
     * 1) Already done
     * 2) Check its compatibility (abilities must match, endTime have to be greater than current time, )
     * 3) Change succesful and doneByHero
     * 4) update mission database
     *
     * @param hero hero to be send.
     * @param mission mission that we want to send hero on
     *
     * @return true if and only if hero is sent on the mission
     *
     * @throws IllegalArgumentException when hero is null, or the mission is null.
     * @throws ServiceFailureException when database operation fails.
     *
     */
    void sendHeroOnMission(Hero hero, Mission mission) throws ServiceFailureException;
    /**
     * Sends hero on specified mission, but only if the mission is not succesful yet and if the ability of the hero
     * match the requirement.
     *
     * 1) Retrieve specified hero and mission from database
     * 2) Check its compatibility (abilities must match, endTime have to be greater than current time, )
     * 3) Change succesful and doneByHero
     * 4) update mission database
     *
     * @param heroID Id of the hero to be send.
     * @param missionID ID of the mission that we want to send hero on
     *
     * @return true if and only if hero is sent on the mission
     *
     * @throws IllegalArgumentException when hero is null, or the mission is null.
     * @throws ServiceFailureException when database operation fails.
     *
     */
    void sendHeroOnMission(Long heroID, Long missionID) throws ServiceFailureException;

    /**
     * List all missions that have been done by specified hero
     *
     * @param hero hero that have done all the returned missions
     * @return list of missions that have been done by specified hero
     *
     * @throws IllegalArgumentException when hero is null.
     * @throws ServiceFailureException when database operation fails.
     *
     */
    List<Mission> findMissionsDoneByHero(Hero hero) throws ServiceFailureException;

    /**
     * List all missions that is specified hero able to complete
     *
     * @param hero hero for whom we are searching appropriate mission
     * @return list of missions that match ability of our hero
     *
     * @throws IllegalArgumentException when hero is null.
     * @throws ServiceFailureException when database operation fails.
     *
     */
    List<Mission> findMissionsForSpecifiedHero(Hero hero) throws ServiceFailureException;

    /**
     * List all heroes that are capable of completing specified mission
     *
     * @param mission mission for which we are searching appropriate heroes
     * @return list of heroes that match required ability of the mission
     *
     * @throws IllegalArgumentException when the mission is null.
     * @throws ServiceFailureException when database operation fails.
     *
     */
    List<Hero> findHeroSuitableForMission(Mission mission) throws ServiceFailureException;

    /**
     * List all missions that are still to be done and have not expired yet
     *
     * @return list of all missions that were not done by eny hero and have not expired.
     *
     * @throws ServiceFailureException when database operation fails.
     *
     */
    List<Mission> listMissionsThatHaveToBeDone() throws ServiceFailureException;
}
