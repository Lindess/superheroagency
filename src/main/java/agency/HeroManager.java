package agency;

import exceptions.ServiceFailureException;

import java.util.List;

/**
 * Created by Tadeas Kucera (423907) on 10.3.2016.
 */
public interface HeroManager {
    /**
     * Stores new hero into database. Id for the new hero is automatically
     * generated and stored into id attribute.
     *
     * @param hero hero to be created.
     * @throws IllegalArgumentException when hero is null, or hero has already assigned id.
     * @throws ServiceFailureException when database operation fails.
     */
    void createHero(Hero hero) throws ServiceFailureException;

    /**
     * Updates hero in database. If the database does not contain this hero, this method does nothing.
     *
     * @param hero hero to be updated in database.
     * @throws IllegalArgumentException when hero is null, or hero has null id.
     * @throws ServiceFailureException when database operation fails
     */
    void updateHero(Hero hero) throws ServiceFailureException;

    /**
     * Deletes hero from database.
     *
     * @param hero hero to be deleted from db.
     * @throws IllegalArgumentException when hero is null, or hero has null id.
     * @throws  ServiceFailureException when db operation fails.
     */
    void deleteHero(Hero hero) throws ServiceFailureException;

    /**
     * Returns list of all heroes in the database.
     *
     * @return list of all heroes in database.
     * @throws  ServiceFailureException when db operation fails.
     */
    List<Hero> findAllHeroes() throws ServiceFailureException;

    /**
     * Retrieve hero with specified ID from the database
     *
     * @param id id of wanted hero
     * @return hero with specified id, or null if there is no such hero in the database
     * @throws  ServiceFailureException when db operation fails.
     */
    Hero findHeroByID(Long id) throws ServiceFailureException;

    /**
     * Retrieve hero with specified nickname from the database
     *
     * @param nick nick of wanted hero
     * @return first hero with specified nickname, or null if there is no such hero in the database
     * @throws  ServiceFailureException when db operation fails.
     */
    Hero findHeroByNick(String nick) throws ServiceFailureException;
}
