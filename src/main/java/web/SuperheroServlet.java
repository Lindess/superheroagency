package web;

import agency.Ability;
import agency.Mission;
import agency.MissionManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
     * Servlet for managing missions.
     *
     **/
    @WebServlet(SuperheroServlet.URL_MAPPING + "/*")
    public class SuperheroServlet extends HttpServlet {

    private static final String LIST_JSP = "/list.jsp";
    public static final String URL_MAPPING = "/superEntities";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        showMissionList(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //support non-ASCII characters in form
        request.setCharacterEncoding("utf-8");
        //action specified by pathInfo
        String action = request.getPathInfo();
        switch (action) {

            case "/add":
                add(request,response);
                break;
            case "/delete":
                delete(request,response);
                break;
            case "/update":
                update(request,response);
                break;
            case "/edit":
                edit(request,response);
                break;
            default:
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "Unknown action " + action);
        }
    }

    /**
     * @return MissionManager instance
     */
    private MissionManager getMissionManager() {
        return (MissionManager) getServletContext().getAttribute("missionManager");
    }

    /**
     * Stores the list of missions to request attribute "mission" and forwards to the JSP to display it.
     */
    private void showMissionList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            request.setAttribute("mission", getMissionManager().listAllMissions());
            request.getRequestDispatcher(LIST_JSP).forward(request, response);
        } catch (Exception e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    private void add(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String abilityRequired = request.getParameter("abilityRequired").toUpperCase();
        String description = request.getParameter("description");
        LocalDate endtime = LocalDate.now().plus(7, ChronoUnit.DAYS);

        if (abilityRequired == null || description.length() == 0 || description == null) {
            request.setAttribute("chyba", "Je nutné vyplnit všechny hodnoty !");
            try {
                showMissionList(request, response);
            } catch (ServletException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }

        try {
            Ability.valueOf(abilityRequired);
        }catch (Exception e){
            request.setAttribute("chyba", "Zadali jste neexistujici schopnost!");
            try {
                showMissionList(request, response);
            } catch (ServletException ex) {
                e.printStackTrace();
            } catch (IOException ex) {
                e.printStackTrace();
            }
            return;
        }
        try {
            Mission mission = new Mission(description, Ability.valueOf(abilityRequired), endtime);
            System.out.println(mission.toString());
            getMissionManager().createMission(mission);
            //redirect-after-POST protects from multiple submission
            response.sendRedirect(request.getContextPath()+URL_MAPPING);
            return;
        } catch (Exception e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
            return;
        }
    }

    private void delete(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            Long id = Long.valueOf(request.getParameter("id"));
            Mission mission = getMissionManager().findMissionByID(id);
            getMissionManager().deleteMission(mission);
            response.sendRedirect(request.getContextPath()+URL_MAPPING);
            return;
        } catch (Exception e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
            return;
        }
    }

    private void update(HttpServletRequest request, HttpServletResponse response) throws IOException {

        Long id = Long.valueOf(request.getParameter("id"));
        Mission mission = getMissionManager().findMissionByID(id);

        String abilityRequired1 = request.getParameter("abilityRequired").toUpperCase();
        String description1 = request.getParameter("description");
        if(!abilityRequired1.isEmpty()){
            try{
                mission.setAbilityRequired(Ability.valueOf(abilityRequired1));
            }catch (Exception e){
                request.setAttribute("chyba", "Zadali jste neexistujici schopnost!");
            }
        }
        if(!description1.isEmpty()){
            mission.setDescription(description1);
        }
        getMissionManager().updateMission(mission);
        response.sendRedirect(request.getContextPath()+URL_MAPPING);
        return;
    }
    private void edit(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Long id = Long.valueOf(request.getParameter("id"));
        Mission m =  getMissionManager().findMissionByID(id);

        request.setAttribute("mission",getMissionManager().listAllMissions());
        request.setAttribute("abilityRequired",m.getAbilityRequired());
        request.setAttribute("description", m.getDescription());
        request.getRequestDispatcher("/update.jsp").forward(request, response);

    }

}
