package web;

import agency.HeroManagerImpl;
import agency.MissionManagerImpl;
import utils.DBManager;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;
import java.sql.SQLException;

@WebListener
public class StartListener implements ServletContextListener {

    private DataSource dataSource;

    @Override
    public void contextInitialized(ServletContextEvent ev) {
        try {
            dataSource = DBManager.prepareDataSource();
            DBManager.tryCreateTables(dataSource);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        MissionManagerImpl missionManager = new MissionManagerImpl();
        missionManager.setDataSource(dataSource);
        HeroManagerImpl heroManager = new HeroManagerImpl();
        heroManager.setDataSource(dataSource);

        ServletContext servletContext = ev.getServletContext();

        servletContext.setAttribute("heroManager", heroManager);
        servletContext.setAttribute("missionManager", missionManager);
    }

    @Override
    public void contextDestroyed(ServletContextEvent ev){
    }
}
