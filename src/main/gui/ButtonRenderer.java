import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * Created by linda on 27.04.2016.
 */
public class ButtonRenderer extends JButton implements TableCellRenderer {

    public ButtonRenderer(){
        setOpaque(true);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object obj, boolean isSelected, boolean hasFocus, int row, int column) {
        setText((obj==null)?"":obj.toString());
        return this;
    }
}

