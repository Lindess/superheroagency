import agency.Mission;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by linda on 27.04.2016.
 */
public class MissionTableModel extends DefaultTableModel {

    private static final String[] COLUMN_NAMES = new String[] {"Ability required", "Description", "Button"};
    private static final Class<?>[] COLUMN_TYPES = new Class<?>[] {String.class, String.class, JButton.class};

    private List<Mission> missions;

    public MissionTableModel() {
        super(COLUMN_NAMES,COLUMN_NAMES.length);
        missions = new ArrayList<Mission>();
    }

    @Override public String getColumnName(int columnIndex) {
        return COLUMN_NAMES[columnIndex];
    }
    @Override public Class<?> getColumnClass(int columnIndex) {
        return COLUMN_TYPES[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (missions == null) return 0;
                else return 3;
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Mission mission = missions.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return mission.getAbilityRequired();
            case 1:
                return mission.getDescription();
            case 2:
                return "Send hero on this mission";
            default:
                throw new IllegalArgumentException("columnIndex");

        }

    }


    public void addMission(Mission mission) {
        missions.add(mission);
    }

}


