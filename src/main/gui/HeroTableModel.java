import agency.Hero;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tadeas Kucera on 30.05.2016.
 */
public class HeroTableModel extends DefaultTableModel {

    private static final String[] COLUMN_NAMES = new String[] {"Ability", "Name", "Button"};
    private static final Class<?>[] COLUMN_TYPES = new Class<?>[] {String.class, String.class, JButton.class};

    private List<Hero> heroes;

    public HeroTableModel() {
        super(COLUMN_NAMES,COLUMN_NAMES.length);
        heroes = new ArrayList<Hero>();
    }

    @Override public String getColumnName(int columnIndex) {
        return COLUMN_NAMES[columnIndex];
    }
    @Override public Class<?> getColumnClass(int columnIndex) {
        return COLUMN_TYPES[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (heroes == null) return 0;
        else return 3;
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Hero hero = heroes.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return hero.getAbility();
            case 1:
                return hero.getName();
            case 2:
                return "Find mission for this hero";
            default:
                throw new IllegalArgumentException("columnIndex");

        }

    }

    public void addHero(Hero hero) {
        heroes.add(hero);
    }

}


