import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by linda on 27.04.2016.
 */
public class ButtonEditor extends DefaultCellEditor {

    protected JButton button;
    private String label;
    private Boolean clicked;

    public ButtonEditor(JTextField textField) {
        super(textField);

        button = new JButton();
        button.setOpaque(true);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //fireEditingStopped();
            }
        });
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object obj, boolean isSelected, int row, int column) {

        label = (obj==null) ? "" : obj.toString();
        button.setText(label);
        clicked = true;
        return button;
    }

    @Override
    public Object getCellEditorValue() {

        if (clicked){
            JOptionPane.showMessageDialog(button,label+"clicked");
        }
        clicked= false;
        return new String(label);
    }

    @Override
    protected void fireEditingStopped() {
        super.fireEditingStopped();
    }
}
