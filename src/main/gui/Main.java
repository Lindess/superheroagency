import javax.swing.*;
import java.awt.*;

/**
 * Created by linda on 26.04.2016.
 */
public class Main {

    public static void main(String[] args) {

        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                JFrame frame = new MainFrame();
                frame.setVisible(true);
            }
        });
    }
}
