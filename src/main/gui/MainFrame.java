import agency.Ability;
import agency.Hero;
import agency.Mission;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.time.LocalDate;

import static java.awt.BorderLayout.CENTER;

/**
 * Created by linda on 26.04.2016.
 */
public class MainFrame extends JFrame{

    private JPanel contentPanel;

    //Header
    private JPanel topPanel;
    private JButton heroButton;
    private JLabel logoLabel;
    private JButton missionButton;

    //Main application window
    private JPanel tableViewPanel;
    private JPanel missionPanel;
    private JPanel heroPanel;


    private JButton addNewMissionButton;
    private JButton addNewHeroButton;
    private JPanel missionBottomPanel;
    private JPanel heroBottomPanel;

    private Label descriptionLabel;
    private Label abilityLabel;

    private JTextField descriptionField;
    private JTextField abilityField;

    //general strings:
    private String ok;
    private String storno;

    //mission strings:
    private String description;
    private String reqiredAbility;
    private String newMission;

    //hero Strings:
    private String newHero;
    private String havingAbility;



    private JTable missionTable;
    private JTable heroTable;



    public MainFrame(){
        super();
        initStrings();
        InitContentPane();

        MissionTableInit();
        HeroTableInit();
        setContentPane(contentPanel);
        setSize(1222, 1070);
        //setMinimumSize(new Dimension(400,400));

    }

    private void initStrings() {
        ok = "Ok";
        storno = "Storno";
        description = "Description";
        reqiredAbility = "Reqired ability";
        newMission = "New mission";
        newHero = "New heroooo";
        havingAbility = "Special ability";
    }


    private void InitContentPane() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        //Head
        topPanel = new JPanel(new GridLayout(1,3));
        topPanel.setBackground(new Color(116,129,150));
        topPanel.setMinimumSize(new Dimension(800,600));
        //Hero body
        heroPanel = new JPanel(new BorderLayout());
        heroPanel.setBackground(new Color(116,129,150));
        heroPanel.setBorder(BorderFactory.createEtchedBorder(Color.black,Color.white));
        //Mission body
        missionPanel = new JPanel(new BorderLayout());
        missionPanel.setBackground(new Color(116,129,150));
        missionPanel.setBorder(BorderFactory.createEtchedBorder(Color.black,Color.white));
        //General body panel
        tableViewPanel = new JPanel(new CardLayout());
        tableViewPanel.add(heroPanel);
        tableViewPanel.add(missionPanel);

        contentPanel = new JPanel(new BorderLayout());
        contentPanel.setBackground(new Color(116,129,150));
        contentPanel.setName("SuperHero Agency");
        contentPanel.add(tableViewPanel,CENTER);
        contentPanel.add(topPanel,BorderLayout.NORTH);

        //Adding components to head
        InitMainButtons();
        InitLogo();
        topPanel.add(heroButton);
        topPanel.add(logoLabel);
        topPanel.add(missionButton);

        //heroPanel.add(new Label("Here will be Hero table..."));
        //missionPanel.ok(new Label("Mission"));


        missionPanel.setVisible(false);
        heroPanel.setVisible(true);

    }

    private void InitLogo(){
        ImageIcon logoIcon = new ImageIcon(this.getClass().getClassLoader().getResource("img/shaLogo2.jpg"),"Superhero Agency");
        logoLabel = new JLabel(){
            @Override
            public void paintComponent (Graphics g) {
                super.paintComponent (g);
                if (logoIcon != null) {
                    g.drawImage (logoIcon.getImage(), CENTER, 0, 400, 500, null);
                }
            }
        };
        logoLabel.setPreferredSize(new Dimension(400,500));
    }

    private void InitMainButtons(){
        ImageIcon hbuttonIcon = new ImageIcon(this.getClass().getClassLoader().getResource("img/herobutton.jpg"));
        heroButton = new JButton(){
            @Override
            public void paintComponent (Graphics g) {
                super.paintComponent (g);
                if (hbuttonIcon != null) {
                    g.drawImage (hbuttonIcon.getImage(), 0, 300, getWidth(), 100, null);
                }
            }
        };
        heroButton.setBackground(new Color(116,129,150));
        heroButton.setMargin(new java.awt.Insets(1, 2, 1, 2));
        heroButton.setPreferredSize(new Dimension(300,100));
        Action switchToHero = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                heroPanel.setVisible(true);
                missionPanel.setVisible(false);
            }
        };
        heroButton.setAction(switchToHero);


        ImageIcon mbuttonIcon = new ImageIcon(this.getClass().getClassLoader().getResource("img/missionbutton.jpg"));
        missionButton = new JButton(){
            @Override
            public void paintComponent (Graphics g) {
                super.paintComponent (g);
                if (mbuttonIcon != null) {
                    g.drawImage (mbuttonIcon.getImage(), 0, 300, getWidth(), 100, null);
                }
            }
        };
        missionButton.setBackground(new Color(116,129,150));
        missionButton.setMargin(new java.awt.Insets(1, 2, 1, 2));
        missionButton.setPreferredSize(new Dimension(300,100));
        Action switchToMission = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                heroPanel.setVisible(false);
                missionPanel.setVisible(true);
            }
        };
        missionButton.setAction(switchToMission);
    }

    private void MissionTableInit(){
        JPanel missionTablePanel = new JPanel(new BorderLayout());

        missionTable = new JTable(new MissionTableModel());

        missionTable.getColumnModel().getColumn(2).setCellRenderer(new ButtonRenderer());
        missionTable.getColumnModel().getColumn(2).setCellEditor(new ButtonEditor(new JTextField()));
        JScrollPane tablePane = new JScrollPane(missionTable);
        missionTable.getTableHeader().setFont(new Font("Times New Roman",1,40));

        missionTablePanel.add(missionTable.getTableHeader(), BorderLayout.NORTH);
        missionTablePanel.add(tablePane, BorderLayout.CENTER);

        missionTable.setRowHeight(50);
        missionTable.setBackground(missionTable.getSelectionBackground());
        missionTable.setPreferredSize(new Dimension(400,800));
        missionTable.setFont(new Font("Times New Roman",1,40));

        MissionTableModel model = (MissionTableModel) missionTable.getModel();
        model.addMission(new Mission("Mission1", Ability.FIRE, LocalDate.MAX));
        model.addMission(new Mission("Mission2", Ability.WATER, LocalDate.MAX));
        model.addMission(new Mission("Mission3", Ability.BOOK, LocalDate.MAX));


        MissionBottomPanelInit();

        missionPanel.add(missionTablePanel, BorderLayout.NORTH);
        missionPanel.add(missionBottomPanel,BorderLayout.CENTER);

    }



    private void MissionBottomPanelInit(){
        CardLayout cards = new CardLayout();
        missionBottomPanel = new JPanel(cards);
        missionBottomPanel.setMinimumSize(new Dimension(1222, 300));

        JPanel addMissionForm = new JPanel(new GridLayout(3, 2));
        JButton okMissionButton;
        JButton stornoMissionButton;

        JPanel addNewMissionButtonPanel = new JPanel(new BorderLayout());

        addNewMissionButton = new JButton();
        Action showForm = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(missionBottomPanel, "addMissionForm");
                setSize(getWidth(), 1200);
            }
        };
        addNewMissionButton.setAction(showForm);
        addNewMissionButton.setText(newMission);
        addNewMissionButtonPanel.add(addNewMissionButton, BorderLayout.CENTER);

        missionBottomPanel.add(addNewMissionButtonPanel, "addNewMissionButton");
        missionBottomPanel.add(addMissionForm, "addMissionForm");

        descriptionLabel = new Label(description);
        descriptionField = new JTextField();

        abilityLabel = new Label(reqiredAbility);
        abilityField = new JTextField();

        okMissionButton = new JButton();
        Action okAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(missionBottomPanel, "addNewMissionButton");
                setSize(getWidth(), 1070);
            }
        };
        okMissionButton.setAction(okAction);
        okMissionButton.setText(ok);

        stornoMissionButton = new JButton();
        Action stornoAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(missionBottomPanel, "addNewMissionButton");
                setSize(getWidth(), 1070);
            }
        };
        stornoMissionButton.setAction(stornoAction);
        stornoMissionButton.setText(storno);

        addMissionForm.add(descriptionLabel);
        addMissionForm.add(descriptionField);
        addMissionForm.add(abilityLabel);
        addMissionForm.add(abilityField);
        addMissionForm.add(okMissionButton);
        addMissionForm.add(stornoMissionButton);

        addMissionForm.setVisible(false);
        addNewMissionButton.setVisible(true);
    }

    private void HeroTableInit(){
        JPanel heroTablePanel = new JPanel(new BorderLayout());

        heroTable = new JTable(new HeroTableModel());


        heroTable.getColumnModel().getColumn(2).setCellRenderer(new ButtonRenderer());
        heroTable.getColumnModel().getColumn(2).setCellEditor(new ButtonEditor(new JTextField()));
        JScrollPane tablePane = new JScrollPane(heroTable);
        heroTable.getTableHeader().setFont(new Font("Times New Roman",1,40));

        heroTablePanel.add(heroTable.getTableHeader(), BorderLayout.NORTH);
        heroTablePanel.add(tablePane, BorderLayout.CENTER);

        heroTable.setRowHeight(50);
        heroTable.setBackground(heroTable.getSelectionBackground());
        heroTable.setPreferredSize(new Dimension(400,800));
        heroTable.setFont(new Font("Times New Roman",1,40));

        HeroTableModel model = (HeroTableModel) heroTable.getModel();
        model.addHero(new Hero("Hero1", "NicknameHero1", Ability.FIRE));
        model.addHero(new Hero("Hero2", "NicknameHero2", Ability.FIRE));
        model.addHero(new Hero("Hero3", "NicknameHero3", Ability.MATH));


        HeroBottomPanelInit();

        heroPanel.add(heroTablePanel, BorderLayout.NORTH);
        heroPanel.add(heroBottomPanel,BorderLayout.CENTER);

    }

    private void HeroBottomPanelInit(){
        CardLayout cards = new CardLayout();
        heroBottomPanel = new JPanel(cards);
        heroBottomPanel.setMinimumSize(new Dimension(1222, 300));

        JPanel addHeroForm = new JPanel(new GridLayout(3, 2));
        JButton okHeroButton;
        JButton stornoHeroButton;

        JPanel addNewHeroButtonPanel = new JPanel(new BorderLayout());

        addNewHeroButton = new JButton();
        Action showForm = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(heroBottomPanel, "addHeroForm");
                setSize(getWidth(), 1200);
            }
        };
        addNewHeroButton.setAction(showForm);
        addNewHeroButton.setText(newHero);
        addNewHeroButtonPanel.add(addNewHeroButton, BorderLayout.CENTER);

        heroBottomPanel.add(addNewHeroButtonPanel, "addNewHeroButton");
        heroBottomPanel.add(addHeroForm, "addHeroForm");

        descriptionLabel = new Label(description);
        descriptionField = new JTextField();

        abilityLabel = new Label(havingAbility);
        abilityField = new JTextField();

        okHeroButton = new JButton();
        Action okAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(heroBottomPanel, "addNewHeroButton");
                setSize(getWidth(), 1070);
            }
        };
        okHeroButton.setAction(okAction);
        okHeroButton.setText(ok);

        stornoHeroButton = new JButton();
        Action stornoAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(heroBottomPanel, "addNewHeroButton");
                setSize(getWidth(), 1070);
            }
        };
        stornoHeroButton.setAction(stornoAction);
        stornoHeroButton.setText(storno);

        addHeroForm.add(descriptionLabel);
        addHeroForm.add(descriptionField);
        addHeroForm.add(abilityLabel);
        addHeroForm.add(abilityField);
        addHeroForm.add(okHeroButton);
        addHeroForm.add(stornoHeroButton);

        addHeroForm.setVisible(false);
        addNewHeroButton.setVisible(true);
    }

}
