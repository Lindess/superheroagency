package agency;


/**
 * This is builder for the {@link Hero} class to make tests better readable.
 *
 * Created by Tadeas Kucera (423907) on 30.3.2016.
 */
public class HeroBuilder {

    private Long id;
    private String name;
    private String nickName;
    private Ability ability;

    public HeroBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public HeroBuilder name(String name) {
        this.name = name;
        return this;
    }

    public HeroBuilder nickName(String nick) {
        this.nickName = nick;
        return this;
    }

    public HeroBuilder ability(Ability ab) {
        this.ability = ab;
        return this;
    }

    public Hero build() {
        Hero hero = new Hero();
        hero.setId(id);
        hero.setNickName(nickName);
        hero.setName(name);
        hero.setAbility(ability);
        return hero;
    }
}