package agency;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import utils.DBManager;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.Month;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Tadeas Kucera (423907) on 27.3.2016.
 */
public class StanLeeManagerImplTest {

    private StanLeeManagerImpl manager;
    private MissionManagerImpl missionManager;
    private HeroManagerImpl heroManager;
    private DataSource ds;

    private final static LocalDate NOW = LocalDate.of(2016, Month.FEBRUARY, 29);
    private final static LocalDate NOTNOW = LocalDate.of(2020, Month.FEBRUARY, 29);

    @Rule
    // attribute annotated with @Rule annotation must be public :-(
    public ExpectedException expectedException = ExpectedException.none();



    @Before
    public void setUp() throws SQLException {
        ds = DBManager.prepareDataSource();
        DBManager.tryCreateTables(ds);

        manager = new StanLeeManagerImpl(ds);
        heroManager = new HeroManagerImpl(ds);
        missionManager = new MissionManagerImpl(ds);
        prepareTestData();
    }

    @After
    public void tearDown() throws SQLException {
        utils.DBManager.executeSqlScript(ds, DBManager.class.getResource("/sqlFiles/deleteTables.sql"));
    }

    //--------------------------------------------------------------------------
    // Preparing test data
    //--------------------------------------------------------------------------


    private Mission m1, m2, m3, m4, m5, m6, m7, missionWithNullId, missionNotInDB;
    private Hero h1, h2, h3, h4, h5, heroWithNullId, heroNotInDB;

    private void prepareTestData() {

        m1 = new MissionBuilder().description("mission 1").abilityRequired(Ability.FIRE).endTime(NOTNOW).build();
        m2 = new MissionBuilder().description("mission 2").abilityRequired(Ability.FIRE).endTime(NOTNOW).build();
        m3 = new MissionBuilder().description("mission 3").abilityRequired(Ability.FIRE).endTime(NOTNOW).build();
        m4 = new MissionBuilder().description("mission 4").abilityRequired(Ability.WATER).endTime(NOTNOW).build();
        m5 = new MissionBuilder().description("mission 5").abilityRequired(Ability.WATER).endTime(NOTNOW).build();
        m6 = new MissionBuilder().description("mission 6").abilityRequired(Ability.WATER).endTime(NOTNOW).build();
        m7 = new MissionBuilder().description("mission 7").abilityRequired(Ability.MATH).endTime(NOTNOW).build();

        h1 = new HeroBuilder().name("Hero 1").nickName("NickName 1").ability(Ability.FIRE).build();
        h2 = new HeroBuilder().name("Hero 2").nickName("NickName 2").ability(Ability.FIRE).build();
        h3 = new HeroBuilder().name("Hero 3").nickName("NickName 3").ability(Ability.WATER).build();
        h4 = new HeroBuilder().name("Hero 4").nickName("NickName 4").ability(Ability.WATER).build();
        h5 = new HeroBuilder().name("Hero 5").nickName("NickName 5").ability(Ability.BOOK).build();

        heroManager.createHero(h1);
        heroManager.createHero(h2);
        heroManager.createHero(h3);
        heroManager.createHero(h4);
        heroManager.createHero(h5);

        missionManager.createMission(m1);
        missionManager.createMission(m2);
        missionManager.createMission(m3);
        missionManager.createMission(m4);
        missionManager.createMission(m5);
        missionManager.createMission(m6);
        missionManager.createMission(m7);

        heroWithNullId = new HeroBuilder().id(null).build();
        heroNotInDB = new HeroBuilder().id(h3.getId() + 111).build();
        assertThat(heroManager.findHeroByID(heroNotInDB.getId())).isNull();
        
        missionWithNullId = new MissionBuilder().description("Mission with null id").id(null).abilityRequired(Ability.MATH).build();
        missionNotInDB = new MissionBuilder().description("Mission not in DB").id(m1.getId() + 111).build();
        assertThat(missionManager.findMissionByID(missionNotInDB.getId())).isNull();
    }

    @Test
    public void testFindHeroSuitableForMission() throws Exception {
        assertThat(manager.findHeroSuitableForMission(m4))
                .usingFieldByFieldElementComparator()
                .containsOnly(h3,h4);
        assertThat(manager.findHeroSuitableForMission(m7)).isEmpty();
    }

    @Test
    public void testFindMissionsDoneByHero() throws Exception {
        assertThat(manager.findMissionsDoneByHero(h1)).isEmpty();
        assertThat(manager.findMissionsDoneByHero(h2)).isEmpty();
        assertThat(manager.findMissionsDoneByHero(h3)).isEmpty();
        assertThat(manager.findMissionsDoneByHero(h4)).isEmpty();
        assertThat(manager.findMissionsDoneByHero(h5)).isEmpty();

        manager.sendHeroOnMission(h1, m1); //h1 does m1
        manager.sendHeroOnMission(h2, m2); //h2 does m2
        manager.sendHeroOnMission(h3, m3); //nothing, incompatible abilities
        manager.sendHeroOnMission(h1, m4); //nothing, incompatible abilities
        manager.sendHeroOnMission(h2, m3); //h2 does m3 (and m2)
      //  manager.sendHeroOnMission(h1, m3); //nothing, already done
        manager.sendHeroOnMission(h4, m4); //h4 does m4
      // manager.sendHeroOnMission(h4, m4); //h4 does m4 again

        assertThat(manager.findMissionsDoneByHero(h5)).isEmpty();
        assertThat(manager.findMissionsDoneByHero(h1))
               // .usingFieldByFieldElementComparator()
                .containsOnly(m1);
        assertThat(manager.findMissionsDoneByHero(h2))
               // .usingFieldByFieldElementComparator()
                .containsOnly(m2,m3);
        assertThat(manager.findMissionsDoneByHero(h3)).isEmpty();
        assertThat(manager.findMissionsDoneByHero(h4))
               // .usingFieldByFieldElementComparator()
                .containsOnly(m4);
    }

    @Test
    public void testSendHeroOnMission() throws Exception {
        assertThat(manager.findMissionsDoneByHero(h1)).isEmpty();
        assertThat(manager.findMissionsDoneByHero(h2)).isEmpty();
        assertThat(manager.findMissionsDoneByHero(h3)).isEmpty();
        assertThat(manager.findMissionsDoneByHero(h4)).isEmpty();
        assertThat(manager.findMissionsDoneByHero(h5)).isEmpty();

        manager.sendHeroOnMission(h1, m1);
        manager.sendHeroOnMission(h2, m2);
        manager.sendHeroOnMission(h3, m3);
        manager.sendHeroOnMission(h1, m4);
        manager.sendHeroOnMission(h2, m3);

        //assertThat(manager.findMissionsDoneByHero(h1))
        //        .usingFieldByFieldElementComparator()
        //        .containsOnly(m1);
        assertThat(manager.findMissionsDoneByHero(h3))
                .isEmpty();
        //assertThat(manager.findMissionsDoneByHero(h2))
        //        .usingFieldByFieldElementComparator()
        //        .containsOnly(m2,m3);
    }

    @Test
    public void testFindMissionsForSpecifiedHero() throws Exception {

        assertThat(manager.findMissionsForSpecifiedHero(h1))
                .usingFieldByFieldElementComparator()
                .containsOnly(m1,m2,m3);

        manager.sendHeroOnMission(h1,m1);

        assertThat(manager.findMissionsForSpecifiedHero(h1))
                .usingFieldByFieldElementComparator()
                .containsOnly(m2,m3);
        assertThat(manager.findMissionsForSpecifiedHero(h2))
                .usingFieldByFieldElementComparator()
                .containsOnly(m2,m3);
    }

    @Test
    public void testListMissionsThatHaveToBeDone() throws Exception {
        assertThat(manager.listMissionsThatHaveToBeDone())
                .usingFieldByFieldElementComparator()
                .containsOnly(m1,m2,m3,m4,m5,m6,m7);

        manager.sendHeroOnMission(h1, m1); //h1 does m1
        manager.sendHeroOnMission(h2, m2); //h2 does m2

        assertThat(manager.listMissionsThatHaveToBeDone())
                .usingFieldByFieldElementComparator()
                .containsOnly(m3,m4,m5,m6,m7);
    }

}