package agency;

import exceptions.EntityNotFoundException;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import utils.DBManager;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

/**
 * Created by Linda Langerova on 16.03.2016.
 */
public class MissionManagerImplTest {

    private MissionManagerImpl manager;
    private DataSource datasource;

    private static final LocalDate ENDTIMEDATE = LocalDate.of(2020,12,12);

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws SQLException {
        datasource = DBManager.prepareDataSource();
        DBManager.tryCreateTables(datasource);

        manager = new MissionManagerImpl(datasource);
    }

    @After
    public void destroy() throws SQLException {
        try (Connection connection = datasource.getConnection()) {
            utils.DBManager.executeSqlScript(datasource, DBManager.class.getResource("/sqlFiles/deleteTables.sql"));
        }
    }

    @Test
    public void createMission(){

        Mission mission = new Mission("Default Mission", Ability.FIRE, ENDTIMEDATE);
        manager.createMission(mission);

        Long missionID = mission.getId();
        assertThat("new mission has null id", missionID , is(not(equalTo(null))));

        Mission result = manager.findMissionByID(missionID);
        assertThat("loaded mission differs from the saved one", result, is(equalTo(mission)));

        assertThat("newly created mission is marked as done", mission.isSuccessful(), is(Boolean.FALSE));
    }

    @Test
    public void getAllMissions() {
        assertThat(manager.listAllMissions().isEmpty());

        Mission m1 = newMission("Mission one", Ability.FIRE, ENDTIMEDATE);
        Mission m2 = newMission("Mission two", Ability.WATER, ENDTIMEDATE);


        manager.createMission(m1);
        manager.createMission(m2);

        assertThat(manager.listAllMissions())
                .usingFieldByFieldElementComparator()
                .containsOnly(m1,m2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createNullMission() {
        manager.createMission(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createMissionWithExistingId() {
        Mission mission = newMission("Mission one", Ability.FIRE, ENDTIMEDATE);
        mission.setId(1l);
        manager.createMission(mission);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createMissionWithNullAbilityRequired() {
        Mission mission = newMission("Mission one", null, ENDTIMEDATE);
        manager.createMission(mission);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createMissionWithNullDescription() {
        Mission mission = newMission(null, Ability.FIRE, ENDTIMEDATE);
        manager.createMission(mission);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createMissionWithNullEndTime() {
        Mission mission = newMission("Mission one", Ability.FIRE, null);
        manager.createMission(mission);
    }

    private void testUpdate(Consumer<Mission> updateOperation) {
        Mission sourceMission = newMission("Mission one", Ability.FIRE, ENDTIMEDATE);
        Mission anotherMission = newMission("Mission two", Ability.WATER, ENDTIMEDATE);
        manager.createMission(sourceMission);
        manager.createMission(anotherMission);

        updateOperation.accept(sourceMission);
        manager.updateMission(sourceMission);

        assertThat(manager.findMissionByID(sourceMission.getId()))
                .isEqualToComparingFieldByField(sourceMission);
        // Check if updates didn't affected other records
        assertThat(manager.findMissionByID(anotherMission.getId()))
                .isEqualToComparingFieldByField(anotherMission);
    }

    @Test
    public void updateMissionDescription() {
        testUpdate((m) -> m.setDescription("New description"));
    }

    @Test
    public void updateMissionAbilityRequirement() {
        testUpdate((m) -> m.setAbilityRequired(Ability.WATER));
    }

    @Test
    public void updateMissionEndTime() {
        testUpdate((m) -> m.setEndTime(ENDTIMEDATE));
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateNullMission() {
        manager.updateMission(null);
    }

    @Test
    public void updateMissionWithNullId() {
        Mission mission = newMission("Mission one", Ability.FIRE, ENDTIMEDATE);
        manager.createMission(mission);
        mission.setId(null);
        expectedException.expect(IllegalArgumentException.class);
        manager.updateMission(mission);
    }

    @Test
    public void updateMissionWithNonExistingId() {
        Mission mission = newMission("Mission one", Ability.FIRE, ENDTIMEDATE);
        manager.createMission(mission);
        mission.setId(mission.getId() + 1);
        expectedException.expect(EntityNotFoundException.class);
        manager.updateMission(mission);
    }

    @Test
    public void updateMissionWithNullAbilityRequired() {
        Mission mission = newMission("Mission one", Ability.FIRE, ENDTIMEDATE);
        manager.createMission(mission);
        mission.setAbilityRequired(null);
        expectedException.expect(IllegalArgumentException.class);
        manager.updateMission(mission);
    }

    @Test
    public void updateMissionWithNullDescription() {
        Mission mission = newMission("Mission one", Ability.FIRE, ENDTIMEDATE);
        manager.createMission(mission);
        mission.setDescription(null);
        expectedException.expect(IllegalArgumentException.class);
        manager.updateMission(mission);
    }

    @Test
    public void updateMissionWithNegativeHeroID() {
        Mission mission = newMission("Mission one", Ability.FIRE, ENDTIMEDATE);
        manager.createMission(mission);
        mission.setDoneByHeroID(-2l);
        expectedException.expect(IllegalArgumentException.class);
        manager.updateMission(mission);
    }

    /**@Test
    public void deleteMission(){

        Mission mission = new Mission("Default Mission", Ability.FIRE, ENDTIMEDATE);
        Mission mission2 = new Mission("Default Mission2", Ability.FIRE, ENDTIMEDATE);

        manager.createMission(mission);

        try{
            manager.deleteMission(null);
            org.junit.Assert.fail("should refuse delete null mission");
        }catch (NullPointerException ex){
        //OK
        }
        try{
            manager.deleteMission(mission2);
            org.junit.Assert.fail("should refuse delete mission not contained by manager");
        }catch (IllegalArgumentException ex){
            //OK
        }
        manager.deleteMission(mission);
        assertEquals("mission should be deleted",manager.findMissionByID(mission.id), null);
    }
     **/

    @Test(expected = IllegalArgumentException.class)
    public void deleteNullMission() {
        manager.deleteMission(null);
    }

    @Test
    public void deleteMissionWithNullId() {
        Mission mission = newMission("Mission one", Ability.FIRE, ENDTIMEDATE);
        mission.setId(null);
        expectedException.expect(IllegalArgumentException.class);
        manager.deleteMission(mission);
    }

    @Test
    public void deleteMissionWithNonExistingId() {
        Mission mission = newMission("Mission one", Ability.FIRE, ENDTIMEDATE);
        mission.setId(1L);
        expectedException.expect(EntityNotFoundException.class);
        manager.deleteMission(mission);
    }

    private static Mission newMission(String description, Ability abilityRequired, LocalDate endTime) {
        Mission mission = new Mission(description, abilityRequired,endTime);
        return mission;
    }
}
