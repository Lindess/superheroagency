package agency;

import java.time.LocalDate;

/**
 * This is builder for the {@link Mission} class to make tests better readable.
 *
 * Created by Tadeas Kucera (423907) on 30.3.2016.
 */

public class MissionBuilder {

    private Long id;
    private String description;
    private Ability abilityRequired;
    private LocalDate endTime;
    private boolean successful;
    private Long doneByHeroID = -1l;

    public MissionBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public MissionBuilder description(String description) {
        this.description = description;
        return this;
    }

    public MissionBuilder abilityRequired(Ability ability) {
        this.abilityRequired = ability;
        return this;
    }
    public MissionBuilder endTime(LocalDate endTime) {
        this.endTime = endTime;
        return this;
    }
    public MissionBuilder successful(boolean b) {
        this.successful = b;
        return this;
    }
    public MissionBuilder doneByHeroID(Long heroID) {
        this.doneByHeroID = heroID;
        return this;
    }

    public Mission build() {
        Mission mission = new Mission();
        mission.setId(id);
        mission.setDescription(description);
        mission.setAbilityRequired(abilityRequired);
        mission.setEndTime(endTime);
        mission.setSuccessful(successful);
        mission.setDoneByHeroID(doneByHeroID);
        return mission;
    }
}