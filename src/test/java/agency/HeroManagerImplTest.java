package agency;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import utils.DBManager;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 * Created by Tadeas Kucera (423907) on 10.3.2016.
 */
public class HeroManagerImplTest {

    private HeroManagerImpl manager;
    private DataSource datasource;

    @Before
    public void setUp() throws SQLException {
        datasource = DBManager.prepareDataSource();
        DBManager.tryCreateTables(datasource);

        manager = new HeroManagerImpl(datasource);
    }

    @After
    public void TearDown() throws SQLException{

        utils.DBManager.executeSqlScript(datasource,DBManager.class.getResource("/sqlFiles/deleteTables.sql"));
    }

        @Test
        public void getAllHeroes() {

            assertTrue(manager.findAllHeroes().isEmpty());

            Hero h1 = new Hero("Jan Meisnar", "DareDevil", Ability.FIRE);
            Hero h2 = new Hero("Joshua Bloch", "CodeWriter", Ability.BOOK);

            manager.createHero(h1);
            manager.createHero(h2);

            List<Hero> expected = Arrays.asList(h1, h2);
            List<Hero> actual = manager.findAllHeroes();

            Collections.sort(actual, idComparator);
            Collections.sort(expected, idComparator);

            assertEquals("saved and retrieved heroes differ", expected, actual);
        }

        @Test
        public void createHero() {
            Hero hero = new Hero("Bruce Wayne", "Batman", Ability.BAT);
            manager.createHero(hero);
            assertThat("Saved hero has null id.", hero.getId(), is(not(equalTo(null))));
            String heroId = hero.getName();

            Hero result = manager.findHeroByID(hero.getId());

            assertThat("loaded hero differs from the saved one", result, is(equalTo(hero)));
            //but it should be another instance
        assertThat("loaded hero is the same instance", result, is(not(sameInstance(hero))));

        // assertEquals("loaded hero is the same instance",hero, result);
        // Hero.equals() method may be broken, check properties' values
    }

    @Test
    public void findHeroByNick() {
        Hero hero = new Hero("Jan Meisnar", "DareDevil", Ability.FIRE);
        manager.createHero(hero);

        String nick = hero.getNickName();
        assertThat("saved hero has null nickname. Should be DareDevil.", nick, is(not(equalTo(null))));

        Hero result = manager.findHeroByNick(nick);

        //assertThat("loaded hero differs from the saved one", result.getName(), is(equalTo(hero.getName())));
        //but it should be another instance
        assertThat("loaded hero is the same instance", result, is(not(sameInstance(hero))));
    }

    @Test
    public void createHeroWithNull(){
        try {
            manager.createHero(null);
            fail("expected IllegalArgumentException for null argument");
        } catch (IllegalArgumentException ex) {
            //OK
        }
    }

    private static Comparator<Hero> idComparator = new Comparator<Hero>(){
        @Override
        public int compare(Hero o1, Hero o2) {
            return o1.getId().compareTo(o2.getId());
        }
    };


}