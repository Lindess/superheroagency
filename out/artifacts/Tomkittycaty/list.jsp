<%@page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<body>

<table border="1">
    <thread>
    <tr>
        <th>abilityRequired</th>
        <th>description</th>
        <th>doneByHero</th>
    </tr>
    </thread>
    <c:forEach items="${mission}" var="mission">
        <tr>
            <td><c:out value="${mission.abilityRequired}"/></td>
            <td><c:out value="${mission.description}"/></td>
            <td><c:out value="${mission.doneByHero}"/></td>
            <td><form method="post" action="${pageContext.request.contextPath}/mission/delete?id=${mission.id}"
                      style="margin-bottom: 0;"><input type="submit" value="Smazat"></form></td>
        </tr>
    </c:forEach>
</table>

<h2>Zadejte misi</h2>
<c:if test="${not empty chyba}">
    <div style="border: solid 1px #ff00f7; background-color: yellow; padding: 10px">
        <c:out value="${chyba}"/>
    </div>
</c:if>
<form action="${pageContext.request.contextPath}/mission/add" method="post">
    <table>
        <tr>
            <th>Název potřebné dovednosti:</th>
            <td><input type="text" name="abilityRequired" value="<c:out value='${param.abilityRequired}'/>"/></td>
        </tr>
        <tr>
            <th>Popis:</th>
            <td><input type="text" name="description" value="<c:out value='${param.description}'/>"/></td>
        </tr>
    </table>
    <input type="Submit" value="Zadat" />
</form>

</body>
</html>